#!/bin/bash
#
set -e
# Signing keys
GPGKEY=41867B2D
DEBFULLNAME="Stefan Peter (Nightly hugin builds)"
DEBEMAIL="hugin@speter.ch"

cd hugin.hg
echo "Pulling in fresh sources"
hg pull
hg update
REVISION=$(hg summary --remote|grep '^parent: '|sed 's/.*: *[^0-9]*\([0-9]*\):[0-9a-z]* .*/\1/g')
REVID=$(hg summary --remote|grep '^parent: '|sed 's/.*: *[^0-9]*[0-9]*:\([0-9a-z]*\) .*/\1/g')
VMAJOR=$(cat CMakeLists.txt |grep 'set(V_MAJOR'|sed 's/set.V_MAJOR \([0-9]*\)./\1/g')
VMINOR=$(cat CMakeLists.txt |grep 'set(V_MINOR'|sed 's/set.V_MINOR \([0-9]*\)./\1/g')
VPATCH=$(cat CMakeLists.txt |grep 'set(V_PATCH'|sed 's/set.V_PATCH \([0-9]*\)./\1/g')
UBREV="0ubuntu1"
PKGVER=${VMAJOR}.${VMINOR}.${VPATCH}+hg${REVISION}
DIRNAME=hugin-${VMAJOR}.${VMINOR}.${VPATCH}
TBName=hugin_${PKGVER}+dfsg.orig.tar.xz
if [ -f ../${TBName} ]; then
    echo "${TBName} alerady exists, no need to redo the tarball"
# Do not mark the tarball for upload
    sOpt="d"
    cd ..
# We may want to nope the old newChangeLog, don't we?
#    rm newChangeLog
#    touch newChangeLog
else
    set +e
    mv ../hugin_*.orig.tar.xz ../lastBuild/
    set -e
#
# Create a directory with the files
#
    echo "Creating fresh tarball"
    if [ -d ../${DIRNAME} ]; then
	rm -Rf ../${DIRNAME}
    fi
    hg archive --rev ${REVISION} --type files ../${DIRNAME}
# create the changelog
    oldrev=$(cat ../lastrev)
    srev=$((${oldrev}+1))
    hg log --branch default -r ${REVISION}:${srev} \
	--template "  * {desc|strip} ({author}, {date|shortdate})\n" | \
	sed 's/\(^\S\)/    \1/g'|grep -v '^$' >../newChangeLog
    cd ..
    numlines=$(cat newChangeLog|wc -l)
    if [ $numlines -gt 0 ]; then
# clean it up accoring to debian dsfg 
	cd ${DIRNAME}
	rm -rf platforms/windows/ mac/*  \
	    src/foreign/getopt/src/msvs/ platforms/mac/* \
	    platforms/CMakeLists.txt
	touch platforms/CMakeLists.txt
	find -name '*.vcproj' -delete
	if [ -d debian ]; then
	    rm -Rf debian
	fi
	rm -Rf .hg*
	echo -n "${REVID}" >rev.txt
	cd ..
# create the new tarball
	tar cJf ${TBName} ${DIRNAME}
# Mark the tarball for upload
	sOpt="a"
    else
	echo "No new history found"
	sOpt="d"
	cd ..
    fi
fi
sOpt="a"
#
# Start the building
# This is ubuntu release specific
for URel in trusty xenial artful bionic; do
    if [ -f lastBuild/hugin_${PKGVER}+dfsg-${UBREV}~${URel}.dsc ]; then
	echo " file lastBuild/hugin_${PKGVER}+dfsg-${UBREV}~${URel}.dsc already exists, no need to redo"
    else
	echo "Start building for ${URel}"
	cd deb
	git checkout nightlies-${URel}
	cd ..
	if [ -d ${DIRNAME} ]; then
	    rm -Rf ${DIRNAME}
	fi
	tar xf ${TBName}
	cd ${DIRNAME}
	if [ -d debian ]; then
            rm -Rf debian
	fi
	cp -r ../deb/debian .
#create a new changelog entry
	echo -n "hugin (${VMAJOR}.${VMINOR}.${VPATCH}+hg${REVISION}" >../tmp
	echo -n "+dfsg-${UBREV}~${URel}) ${URel}; " >>../tmp
	echo "urgency=medium" >>../tmp
	echo "" >>../tmp
	echo -e "  * Nightly build of hugin.${VMAJOR}.${VMINOR}.${VPATCH}.${REVID} for ${URel}">>../tmp
	cat ../newChangeLog >>../tmp
	echo "" >>../tmp
	echo -n " -- ${DEBFULLNAME} <${DEBEMAIL}>  ">>../tmp
	date -R >>../tmp
	echo "" >>../tmp
	cat debian/changelog >>../tmp
	cp ../tmp debian/changelog
	rm ../tmp
	debuild -S -s${sOpt}
	cp -r debian/* ../deb/debian/
	cd ../deb
	git commit -a -m "Nightly build for ${URel}"
	git push origin nightlies-${URel}
# Transmit to launchpad
	sOpt="d"
	cd ..
	dput hugin-nightly hugin_${VMAJOR}.${VMINOR}.${VPATCH}+hg${REVISION}+dfsg-${UBREV}~${URel}_source.changes
    fi
done


#
# clean up
#
echo "Cleaning up"

echo -n "${REVISION}">lastrev
if [ ! -d lastBuild ]; then
    mkdir lastBuild
fi
for f in hugin_${PKGVER}+dfsg-${UBREV}~*; do
    if [ -f $f ]; then
	mv ${f} lastBuild/
    fi
done
find lastBuild -ctime +30 -exec rm \{\} \;
echo "done"
